<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jabatan extends Model
{
    protected $table = 'jabatan';

    protected $fillable = [
        'jabatan_name',
        'access_knowledge_document',
        'upload_knowledge_document',
        'download_knowledge_document',
        'verification_knowledge_document',
        'create_channel_discussion',
        'verification_channel_discussion',
        'manage_knowledge_document',
        'manage_channel_discussion'
    ];
}
