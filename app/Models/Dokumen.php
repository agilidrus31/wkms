<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dokumen extends Model
{
    protected $table = 'dokumen';
    protected $fillable = [
        'kategori_fk',
        'nama_dokumen',
        'url_dokumen',
        'user_fk',
        'is_verified'
    ];
}
