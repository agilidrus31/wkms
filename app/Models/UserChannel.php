<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserChannel extends Model
{
    protected $table = 'users_channel';
    protected $fillable = [
        'channel_fk',
        'user_fk',
        'status'
    ];
}
