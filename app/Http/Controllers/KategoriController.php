<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Kategori::all();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '
                    <form action="' . route('kategori.destroy', $row->id) . '" method="POST" id="delete-form">
                    ' . csrf_field() . '
                    ' . method_field('DELETE') . '
                        <a href="' . route('kategori.edit', $row->id) . '" data-toggle="tooltip" title="Edit Jabatan" class="btn btn-warning btn-circle btn-sm edit"><i class="fas fa-edit"></i></a>
                        <button type="submit" id="btn-submit" class="btn btn-danger btn-circle btn-sm" data-toggle="tooltip" title="Hapus Jabatan">
                            <i class="fas fa-trash"></i>
                        </button>
                    </form>
                    ';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('kategori.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kategori.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'nama_kategori' => 'required|unique:kategori,category_name'
        ]);

        if ($validate->fails()) {
            return redirect()->back()->with('error', $validate->errors()->first())->withInput();
        }

        $category = Kategori::create([
            'category_name' => $request->nama_kategori
        ]);

        return redirect()->route('kategori.index')->with('success', 'berhasil membuat kategori');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = Kategori::find($id);

        return view('kategori.edit', compact('kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = Validator::make($request->all(), [
            'nama_kategori' => 'required|unique:kategori,category_name'
        ]);

        if ($validate->fails()) {
            return redirect()->back()->with('error', $validate->errors()->first())->withInput();
        }

        $category = Kategori::findOrFail($id);
        $category->update([
            'category_name' => $request->nama_kategori
        ]);

        return redirect()->route('kategori.index')->with('success', 'berhasil update kategori');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kategori = Kategori::findOrFail($id);
        $kategori->delete();

        return redirect()->route('kategori.index')->with('success', 'berhasil hapus kategori');
    }
}
