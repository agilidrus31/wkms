<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login()
    {
        return view('auth.login');
    }

    public function authenticating(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required'
        ], [
            'username.required' => 'Username harus diisi.',
            'password.required' => 'Password harus diisi.'
        ]);

        if ($validate->fails()) {
            return redirect()->back()->with('error', $validate->errors()->first())->withInput();
        }

        $authentication = Auth::attempt(['username' => $request->username, 'password' => $request->password]);

        if (!$authentication) {
            return redirect()->back()->with('error', 'Username atau password salah.')->withInput();
        }

        return redirect()->intended('/');
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('login');
    }
}
