<?php

namespace App\Http\Controllers;

use App\Models\Dokumen;
use App\Models\Jabatan;
use App\Models\Kategori;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class DokumenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $jabatan_fk = Auth::user()->jabatan_fk;
        $jabatan = Jabatan::find($jabatan_fk);
        $kategori = Kategori::all();
        if ($request->ajax()) {
            $jabatanName = strtolower($jabatan->jabatan_name);
            $data = Dokumen::join('kategori', 'dokumen.kategori_fk', '=', 'kategori.id')
                ->join('users', 'dokumen.user_fk', '=', 'users.id')
                ->join('jabatan', 'users.jabatan_fk', '=', 'jabatan.id')
                ->where('kategori_fk', 'like', '%' . $request->input('kategori') . '%');

            if ($jabatanName != 'karyawan' && $jabatanName != 'supervisor') {
                $data->where('is_verified', 'like', '%' . $request->input('status') . '%%');
            } else {
                $data->where('is_verified', 1);
            }

            if ($request->input('stDate') != null  && $request->input('endDate')) {
                $stDate = Carbon::parse($request->input('stDate'))->startOfDay();
                $endDate = Carbon::parse($request->input('endDate'))->endOfDay();

                $data->whereBetween('created_at', [$stDate, $endDate]);
            }

            $data = $data->select(
                'dokumen.id as docs_id',
                'dokumen.nama_dokumen',
                'dokumen.url_dokumen',
                'dokumen.is_verified',
                'dokumen.created_at as dokumen_created_at',
                'kategori.category_name',
                'users.name as created_by',
                'jabatan.*'
            )->get();

            return DataTables::of($data)
                ->addIndexColumn()
                ->editColumn('is_verified', function ($row) {
                    if ($row->is_verified) {
                        $btn = '<span class="badge badge-success">Sudah diverifikasi</span>';
                    } else {
                        if ($row->verification_knowledge_document) {
                            $btn = '<a href="' . route('dokumen.verify', $row->docs_id) . '" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Verifikasi Dokumen">
                                    Verifikasi
                                </a>';
                        } else {
                            $btn = '<span class="badge badge-warning">Akses dibatasi!</span>';
                        }
                    }

                    return $btn;
                })
                ->addColumn('download', function ($row) {
                    if ($row->download_knowledge_document && $row->is_verified) {
                        $btn = '
                        <a href="' . $row->url_dokumen . '" class="btn btn-success btn-circle btn-sm" data-toggle="tooltip" title="Download Dokumen">
                            <i class="fas fa-download"></i>
                        </a>
                        ';
                    } else {
                        $btn = '<span class="badge badge-warning">Akses dibatasi!</span>';
                    }
                    return $btn;
                })
                ->addColumn('action', function ($row) {
                    if ($row->is_verified) {
                        $btn = '
                        <form action="' . route('dokumen.destroy', $row->docs_id) . '" method="POST" id="delete-form">
                        ' . csrf_field() . '
                        ' . method_field('DELETE') . '
                            <button type="button" class="btn btn-primary btn-circle btn-sm btnShow" data-id="' . $row->docs_id . '" data-toggle="tooltip" title="Lihat Dokumen">
                                <i class="fas fa-eye"></i>
                            </button>
                        ';

                            if ($row->manage_knowledge_document) {
                                $btn .= '
                            <button type="button" class="btn btn-danger btn-circle btn-sm btnSubmit" data-toggle="tooltip" title="Hapus Dokumen">
                                <i class="fas fa-trash"></i>
                            </button>
                            ';
                            }

                            $btn .= '</form>';
                    } else {
                        $btn = '<span class="badge badge-warning">Akses dibatasi!</span>';
                    }
                    return $btn;
                })
                ->rawColumns(['is_verified', 'download', 'action'])
                ->make(true);
        }

        return view('dokumen.index', compact('jabatan', 'kategori'));
    }

    public function verify($id)
    {
        $dokumen = Dokumen::findOrFail($id);
        $dokumen->update(['is_verified' => 1]);

        return redirect()->route('dokumen.index')->with('success', 'Dokumen berhasil di verifikasi.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Kategori::all();
        return view('dokumen.create', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'nama_dokumen' => 'required',
            'kategori' => 'required',
            'file' => 'required|file'
        ]);

        if ($validate->fails()) {
            return redirect()->back()->with('error', $validate->errors()->first())->withInput();
        }

        $file = $request->file('file');
        $namafile = str_replace(' ', '_', strtolower($request->nama_dokumen));
        $path = $namafile . '.' . $file->getClientOriginalExtension();
        $file->move(public_path('uploads/document'), $path);

        Dokumen::create([
            'kategori_fk' => $request->kategori,
            'nama_dokumen' => $request->nama_dokumen,
            'url_dokumen' => url('uploads/document/' . $path),
            'user_fk' => Auth::user()->id,
            'is_verified' => false
        ]);

        return redirect()->route('dokumen.index')->with('success', 'Dokumen berhasil ditambahkan.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dokumen = Dokumen::find($id);
        $previewUrl = 'https://docs.google.com/viewer?url=' . urlencode($dokumen->url_dokumen) . '&embedded=true';

        return response()->json(['url' => $previewUrl]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dokumen = Dokumen::findOrFail($id);
        $dokumen->delete();

        return redirect()->route('dokumen.index')->with('success', 'Data berhasil dihapus');
    }

    public function documentVerify($id)
    {
        $dokumen = Dokumen::findOrFail($id);
        $dokumen->update(['is_verified' => 1]);

        return redirect()->route('dokumen.index')->with('success', 'Dokumen berhasil di verifikasi.');
    }
}
