<?php

namespace App\Http\Controllers;

use App\Models\Jabatan;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class JabatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Jabatan::all();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '
                    <form action="' . route('jabatan.destroy', $row->id) . '" method="POST" id="delete-form">
                    ' . csrf_field() . '
                    ' . method_field('DELETE') . '
                        <a href="' . route('jabatan.edit', $row->id) . '" data-toggle="tooltip" title="Edit Jabatan" class="btn btn-warning btn-circle btn-sm edit"><i class="fas fa-edit"></i></a>
                        <button type="submit" id="btn-submit" class="btn btn-danger btn-circle btn-sm" data-toggle="tooltip" title="Hapus Jabatan">
                            <i class="fas fa-trash"></i>
                        </button>
                    </form>
                    ';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('jabatan.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('jabatan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'jabatan_name' => 'required|unique:jabatan,jabatan_name'
        ]);

        if ($validate->fails()) {
            return redirect()->back()->with('error', $validate->errors()->first())->withInput();
        }

        try {
            $jabatan = Jabatan::create([
                'jabatan_name' => $request->jabatan_name,
                'access_knowledge_document' => $request->access_knowledge_document ? true : false,
                'upload_knowledge_document' => $request->upload_knowledge_document ? true : false,
                'download_knowledge_document' => $request->download_knowledge_document ? true : false,
                'verification_knowledge_document' => $request->verification_knowledge_document ? true : false,
                'create_channel_discussion' => $request->create_channel_discussion ? true : false,
                'verification_channel_discussion' => $request->verification_channel_discussion ? true : false,
                'manage_knowledge_document' => $request->manage_knowledge_document ? true : false,
                'manage_channel_discussion' => $request->manage_channel_discussion ? true : false
            ]);

            return redirect()->route('jabatan.index')->with('success', 'Jabatan sudah ditambahkan');
        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jabatan = Jabatan::find($id);

        return view('jabatan.edit', compact('jabatan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = Validator::make($request->all(), [
            'jabatan_name' => 'required'
        ]);

        if ($validate->fails()) {
            return redirect()->back()->with('error', $validate->errors()->first())->withInput();
        }

        try {
            $jabatan = Jabatan::findOrFail($id);
            $jabatan->update([
                'jabatan_name' => $request->jabatan_name,
                'access_knowledge_document' => $request->access_knowledge_document ? true : false,
                'upload_knowledge_document' => $request->upload_knowledge_document ? true : false,
                'download_knowledge_document' => $request->download_knowledge_document ? true : false,
                'verification_knowledge_document' => $request->verification_knowledge_document ? true : false,
                'create_channel_discussion' => $request->create_channel_discussion ? true : false,
                'verification_channel_discussion' => $request->verification_channel_discussion ? true : false,
                'manage_knowledge_document' => $request->manage_knowledge_document ? true : false,
                'manage_channel_discussion' => $request->manage_channel_discussion ? true : false
            ]);

            return redirect()->route('jabatan.index')->with('success', 'Jabatan sudah diupdate');
        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jabatan = Jabatan::findOrFail($id);
        $jabatan->delete();
        return redirect()->route('jabatan.index')->with('success', 'Jabatan sudah dihapus');
    }
}
