<?php

namespace App\Http\Controllers;

use App\Models\Channel;
use App\Models\Jabatan;
use App\Models\Message;
use App\Models\Tags;
use App\Models\User;
use App\Models\UserChannel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DiskusiController extends Controller
{
    public function index(Request $request)
    {
        $channel_id = $request->get('channel');
        $tags_id = $request->get('tag');
        $channels = Channel::all();
        $jabatan = Jabatan::find(Auth::user()->jabatan_fk);
        $jabatans = Jabatan::all();
        $users = User::all();
        $tags = [];
        $users_channel = [];
        $isJoin = false;
        $isOwner = false;
        $totalUser = 0;

        if ($channel_id != null) {
            $userChannel = UserChannel::where('channel_fk', $channel_id)->where('user_fk', Auth::user()->id)->first();
            if ($userChannel) {
                if ($userChannel->status == 0) {
                    $isJoin = false;
                } else if ($userChannel->status == 1) {
                    $isJoin = true;
                } else if ($userChannel->status == 2) {
                    $isJoin = true;
                    $isOwner = true;
                }
            }
            $tags = Tags::where('channel_fk', $channel_id)->get();
            $totalUser = UserChannel::where('channel_fk', $channel_id)
                        ->whereIn('status', [1, 2])
                        ->count();
            $users_channel = UserChannel::join('users', 'users_channel.user_fk', '=', 'users.id')
            ->where('users_channel.channel_fk', $channel_id)
            ->whereIn('users_channel.status', [1, 2])
            ->select('users.*')
            ->get();
        }

        return view('diskusi.index', compact(
            'channel_id',
            'tags_id',
            'channels',
            'tags',
            'jabatan',
            'isOwner',
            'isJoin',
            'jabatans',
            'users',
            'totalUser',
            'users_channel'
        ));
    }

    public function getMessage(Request $request)
    {
        $channel_id = $request->channel;
        $tags_id = $request->tags;

        $messages = Message::join('users', 'messages.user_fk', '=', 'users.id')
            ->where('channel_fk', $channel_id)
            ->where('tags_fk', $tags_id)
            ->select(
                'users.name as user_name',
                'messages.*'
            )
            ->get();

        return response()->json(['message' => $messages]);
    }

    public function sentMessage(Request $request)
    {
        $channel_id = $request->channel;
        $tags_id = $request->tags;
        $user_id = Auth::user()->id;

        Message::create([
            'channel_fk' => $channel_id,
            'tags_fk' => $tags_id,
            'user_fk' => $user_id,
            'message' => $request->message
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Pesan dikirim'
        ]);
    }

    public function createChannel(Request $request)
    {
        $nama_channel = $request->nama_channel;
        $user_id = Auth::user()->id;

        Channel::create(['channel_name' => $nama_channel, 'user_fk' => $user_id]);
        $channel = Channel::where('channel_name', $nama_channel)->first();
        UserChannel::create(['channel_fk' => $channel->id, 'user_fk' => $user_id, 'status' => 2]);

        return redirect()->back()->with('success', 'berhasil membuat channel');
    }

    public function addUser(Request $request)
    {
        $channel_id = $request->channel;
        $user_id = $request->user;
        $jabatan_id = $request->jabatan;

        UserChannel::create(['channel_fk' => $channel_id, 'user_fk' => $user_id, 'status' => 1]);

        return redirect()->back()->with('success', 'berhasil menambah anggota');
    }

    public function createTag(Request $request)
    {
        $channel_id = $request->channel;

        Tags::create(['tags_name' => $request->nama_tag, 'channel_fk' => $channel_id]);

        return redirect()->back()->with('success', 'berhasil menambah tag');
    }
}
