<?php

namespace App\Http\Controllers;

use App\Models\Dokumen;
use App\Models\Message;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $totalUser = User::count();
        $totalDokumen = Dokumen::count();
        $totalMessage = Message::count();
        return view('dashboard.index', compact('totalUser', 'totalDokumen', 'totalMessage'));
    }
}
