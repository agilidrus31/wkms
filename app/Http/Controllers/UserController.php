<?php

namespace App\Http\Controllers;

use App\Models\Jabatan;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = User::join('jabatan', 'users.jabatan_fk', '=', 'jabatan.id')
            ->where('users.jabatan_fk', 'like', '%' . $request->jabatan . '%')
            ->select(
                'users.*',
                'jabatan.jabatan_name'
            )
            ->orderBy('users.created_at', 'DESC')
            ->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '
                    <form action="' . route('user.destroy', $row->id) . '" method="POST" id="delete-form">
                    ' . csrf_field() . '
                    ' . method_field('DELETE') . '
                        <a href="' . route('user.edit', $row->id) . '" data-toggle="tooltip" title="Edit Jabatan" class="btn btn-warning btn-circle btn-sm edit"><i class="fas fa-edit"></i></a>
                        <button type="submit" id="btn-submit" class="btn btn-danger btn-circle btn-sm" data-toggle="tooltip" title="Hapus Jabatan">
                            <i class="fas fa-trash"></i>
                        </button>
                    </form>
                    ';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        $jabatan = Jabatan::all();
        return view('user.index', compact('jabatan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jabatan = Jabatan::all();
        return view('user.create', compact('jabatan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'name' => 'required|max:100',
            'username' => 'required|max:100|unique:users,username',
            'email' => 'required|email|max:200',
            'password' => 'required|min:8',
            'jabatan' => 'required'
        ]);

        if ($validate->fails()) {
            return redirect()->back()->with('error', $validate->errors()->first())->withInput();
        }

        try {
            User::create([
                'name' => $request->name,
                'username' => $request->username,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'jabatan_fk' => $request->jabatan
            ]);

            return redirect()->route('user.index')->with('success', 'Berhasil membuat user.');
        } catch (Exception $e) {
            return redirect()->back()->with('error', $e->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $jabatan = Jabatan::all();

        return view('user.edit', compact('user', 'jabatan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = Validator::make($request->all(), [
            'name' => 'required|max:100',
            'username' => 'required|max:100',
            'email' => 'required|email|max:200',
            // 'password' => 'required|min:8',
            'jabatan' => 'required'
        ]);

        if ($validate->fails()) {
            return redirect()->back()->with('error', $validate->errors()->first())->withInput();
        }

        $user = User::findOrFail($id);
        $user->name = $request->name;
        $user->username =  $request->username;
        $user->email = $request->email;
        if (isset($request->password)) {
            $user->password = $request->password;
        }
        $user->jabatan_fk = $request->jabatan;

        $user->save();

        return redirect()->route('user.index')->with('success', 'Success update data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->route('user.index')->with('success', 'Success delete data');
    }
}
