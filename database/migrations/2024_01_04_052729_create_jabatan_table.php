<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJabatanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jabatan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('jabatan_name', 100);
            $table->boolean('access_knowledge_document')->default(false);
            $table->boolean('upload_knowledge_document')->default(false);
            $table->boolean('download_knowledge_document')->default(false);
            $table->boolean('verification_knowledge_document')->default(false);
            $table->boolean('create_channel_discussion')->default(false);
            $table->boolean('verification_channel_discussion')->default(false);
            $table->boolean('manage_knowledge_document')->default(false);
            $table->boolean('manage_channel_discussion')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jabatan');
    }
}
