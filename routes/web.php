<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', 'AuthController@login')->name('login');
Route::post('authenticating', 'AuthController@authenticating')->name('authenticating');

Route::group(['middleware' => ['auth']], function() {
    Route::post('logout', 'AuthController@logout')->name('logout');
    Route::get('/', 'DashboardController@index')->name('dashboard.index');
    Route::resource('user', 'UserController');
    Route::resource('jabatan', 'JabatanController');
    Route::resource('kategori', 'KategoriController');
    Route::resource('dokumen', 'DokumenController');
    Route::get('dokumen/verify/{id}', 'DokumenController@verify')->name('dokumen.verify');
    Route::get('diskusi', 'DiskusiController@index')->name('diskusi.index');
    Route::post('diskusi/getMessage', 'DiskusiController@getMessage')->name('diskusi.getMessage');
    Route::post('diskusi/sentMessage', 'DiskusiController@sentMessage')->name('diskusi.sentMessage');
    Route::post('diskusi/createChannel', 'DiskusiController@createChannel')->name('diskusi.createChannel');
    Route::post('diskusi/addpartisipant', 'DiskusiController@addUser')->name('diskusi.addPartisipant');
    Route::post('diskusi/createTag', 'DiskusiController@createTag')->name('diskusi.createtag');
});
