@extends('layout.app')

@section('title', 'Create Jabatan - ' . env('APP_NAME'))

@section('headBody')
    @include('includes.breadcrumb', [
        'title' => 'Create Jabatan',
    ])
@endsection

@push('styles')
@endpush

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <form action="{{ route('jabatan.store') }}" method="POST">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label>Nama Jabatan</label>
                            <input type="text" class="form-control" name="jabatan_name" placeholder="Masukkan nama jabatan">
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="access_knowledge_document">
                                        <label class="form-check-label">Access Knowledge Document</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="upload_knowledge_document">
                                        <label class="form-check-label">Upload Knowledge Document</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="download_knowledge_document">
                                        <label class="form-check-label">Download Knowledge Document</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="verification_knowledge_document">
                                        <label class="form-check-label">Verification Knowledge Document</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="create_channel_discussion">
                                        <label class="form-check-label">Create Channel Discussion</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="verification_channel_discussion">
                                        <label class="form-check-label">Verification Channel Discussion</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="manage_knowledge_document">
                                        <label class="form-check-label">Manage Knowledge Document</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="manage_channel_discussion">
                                        <label class="form-check-label">Manage Channel Discussion</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="{{ route('jabatan.index') }}" class="btn btn-warning">Batal</a>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
@endpush
