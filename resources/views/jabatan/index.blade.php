@extends('layout.app')

@section('title', 'Jabatan - ' . env('APP_NAME'))

@section('headBody')
    @include('includes.breadcrumb', [
        'title' => 'Jabatan',
    ])
@endsection

@push('styles')
@endpush

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="btn-group" style="margin-bottom: 1rem;">
                <a href="{{ route('jabatan.create') }}" class="btn btn-primary btn-sm" style="width: 8rem;">
                    <i class="fa fa-plus"></i> &nbsp; &nbsp;
                    Add Data
                </a>
            </div>
            <div class="card">
                <div class="card-header">
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-head-fixed text-nowrap table-striped table-hover datatable">
                        <thead>
                            <th>
                                #
                            </th>
                            <th>Nama Jabatan</th>
                            <th>Aksi</th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(function() {
            var table = $('.datatable').DataTable({
                searching: false,
                destroy: true,
                processing: false,
                ordering: false,
                serverSide: true,
                ajax: {
                    url: "{{ route('jabatan.index') }}",
                    data: function(d) {
                        (d.jabatan = $('#jabatan').val())
                    },
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'jabatan_name',
                        name: 'jabatan_name'
                    },
                    {
                        data: 'action',
                        name: 'action'
                    }
                ],
            });

            @if (session('success'))
                Swal.fire({
                    icon: 'success',
                    title: 'Success...',
                    text: '{{ session('success') }}'
                });
            @endif

            @if (session('error'))
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: '{{ session('error') }}'
                });
            @endif
        });
    </script>
@endpush
