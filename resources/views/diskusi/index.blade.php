@extends('layout.app')

@section('title', 'Diskusi - ' . env('APP_NAME'))

@section('headBody')
    @include('includes.breadcrumb', [
        'title' => 'Diskusi',
    ])
@endsection

@push('styles')
@endpush

@section('content')
    <div class="row">
        <div class="col-lg-3">
            <div class="btn-group" style="margin-bottom: 1rem;">
                @if ($jabatan->create_channel_discussion)
                    <a href="#modalCreateChannel" class="btn btn-primary btn btn-sm" data-toggle="modal">
                        <i class="fa fa-plus"></i> &nbsp; &nbsp;
                        Buat Channel
                    </a>
                @endif
                @if ($channel_id != null)
                    @if ($isOwner && $jabatan->manage_channel_discussion && $isJoin)
                        <a href="#modalAddPartisipant" class="btn btn-info btn btn-sm" data-toggle="modal">
                            <i class="fa fa-plus"></i> &nbsp; &nbsp;
                            Tambah Participant
                        </a>
                    @else
                        <a href="" class="btn btn-info btn btn-sm" id="btnAdd">
                            <i class="fa fa-plus"></i> &nbsp; &nbsp;
                            Join Ke Channel
                        </a>
                    @endif
                @endif
            </div>
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Semua Channel</h3>
                </div>
                <div class="card-body p-0">
                    <ul class="nav nav-pills flex-column">
                        @forelse ($channels as $c)
                            <li class="nav-item {{ $channel_id == $c->id ? 'active' : '' }}">
                                <a href="{{ route('diskusi.index', ['channel' => $c->id]) }}"
                                    class="nav-link {{ $channel_id == $c->id ? 'active' : '' }}">
                                    {{ $c->channel_name }}
                                </a>
                            </li>
                        @empty
                            <li class="nav-item">
                                <span class="nav-link">Belum ada channel tersedia</span>
                            </li>
                        @endforelse
                    </ul>
                </div>
            </div>
            @if ($channel_id != null)
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-xl">
                    <i class="fa fa-users"></i> &nbsp; &nbsp;
                    Total User: {{ $totalUser }}
                </button>
            @endif
        </div>
        @if ($channel_id != null)
            <div class="col-lg-3">
                <div class="btn-group" style="margin-bottom: 1rem;">
                    @if ($jabatan->manage_channel_discussion)
                        <a href="#modalBuatTag" class="btn btn-primary btn btn-sm" data-toggle="modal" style="width: 8rem;">
                            <i class="fa fa-plus"></i> &nbsp; &nbsp;
                            Buat Tags
                        </a>
                    @endif
                </div>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Semua Tag</h3>
                    </div>
                    <div class="card-body p-0">
                        <ul class="nav nav-pills flex-column">
                            @forelse ($tags as $t)
                                <li class="nav-item {{ $tags_id == $t->id ? 'active' : '' }}">
                                    <a href="{{ route('diskusi.index', ['channel' => $channel_id, 'tag' => $t->id]) }}"
                                        class="nav-link {{ $tags_id == $t->id ? 'active' : '' }}">
                                        {{ $t->tags_name }}
                                    </a>
                                </li>
                            @empty
                                <li class="nav-item">
                                    <span class="nav-link">Belum ada tag tersedia</span>
                                </li>
                            @endforelse
                        </ul>
                    </div>
                </div>
            </div>
        @endif

        @if ($channel_id != null && $tags_id != null)
            <div class="col-lg-6">
                <div class="card direct-chat direct-chat-primary">
                    <div class="card-header">
                        <h3 class="card-title" id="tagName"></h3>
                    </div>
                    <div class="card-body">
                        <div class="direct-chat-messages"></div>
                    </div>
                    <div class="card-footer">
                        <div class="input-group">
                            <input type="text" name="message" id="message" placeholder="Type Message ..."
                                class="form-control"
                                value="{{ !$isOwner && !$isJoin ? 'Anda belum bergabung ke channel ini.' : '' }}"
                                {{ !$isOwner && !$isJoin ? 'disabled' : '' }}>
                            <span class="input-group-append">
                                <button type="button" class="btn btn-primary" id="btn-send-message"
                                    {{ !$isOwner && !$isJoin ? 'disabled' : '' }}>Send</button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>

    <div class="modal fade" id="modalCreateChannel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{ route('diskusi.createChannel') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">
                            Buat Channel Baru
                        </h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nama Channel</label>
                            <input type="text" class="form-control" id="nama_channel" name="nama_channel"
                                placeholder="Masukka nama channel">
                        </div>
                    </div>
                    <div class="modal-footer" id="modalFooter">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">
                            Cancel
                        </button>
                        <button class="btn btn-primary" type="submit" id="btnCreateChannel">
                            Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalAddPartisipant" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{ route('diskusi.addPartisipant') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">
                            Tambah Partisipant
                        </h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="channel" value="{{ $channel_id }}">
                        <div class="form-group">
                            <label>Jabatan</label>
                            <select name="jabatan" class="form-control" id="">
                                <option value="">--pilih jabatan--</option>
                                @foreach ($jabatans as $j)
                                    <option value="{{ $j->id }}">{{ $j->jabatan_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>User</label>
                            <select name="user" class="form-control" id="">
                                <option value="">--pilih user--</option>
                                @foreach ($users as $j)
                                    <option value="{{ $j->id }}">{{ $j->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer" id="modalFooter">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">
                            Cancel
                        </button>
                        <button class="btn btn-primary" type="submit" id="btnCreateChannel">
                            Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalBuatTag" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{ route('diskusi.createtag') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">
                            Buat Tag Baru
                        </h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="channel" value="{{ $channel_id }}">
                        <div class="form-group">
                            <label>Nama Tag</label>
                            <input type="text" class="form-control" id="nama_tag" name="nama_tag"
                                placeholder="Masukkan nama tag">
                        </div>
                    </div>
                    <div class="modal-footer" id="modalFooter">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">
                            Cancel
                        </button>
                        <button class="btn btn-primary" type="submit" id="btnCreateChannel">
                            Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-xl">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Participant List</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Username</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users_channel as $uc)
                            <tr>
                                <td>{{ $uc->name }}</td>
                                <td>{{ $uc->username }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>

    </div>

@endsection

@push('scripts')
    <script>
        $(function() {
            let channel_id = "{{ $channel_id }}";
            let tags_id = "{{ $tags_id }}";
            let user_id = "{{ Auth::user()->id }}";

            if (channel_id != "" && tags_id != "") {
                syncMessage();
                setInterval(() => {
                    syncMessage();
                }, 1000);
            }

            function syncMessage() {
                $.ajax({
                    type: 'POST',
                    url: '{{ route('diskusi.getMessage') }}',
                    data: {
                        channel: channel_id,
                        tags: tags_id
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr("content")
                    },
                    success: function(response) {
                        var chatContainer = $('.direct-chat-messages');
                        chatContainer.empty();
                        $.each(response.message, function(index, item) {
                            const inputTime = new Date(item.created_at);
                            const options = {
                                day: 'numeric',
                                month: 'short',
                                hour: '2-digit',
                                minute: '2-digit',
                                hour12: true
                            };
                            const formattedTime = inputTime.toLocaleString('en-US', options);
                            var newMessage = `
                                <div class="direct-chat-msg ${user_id == item.user_fk ? 'right' : ''}">
                                    <div class="direct-chat-infos clearfix">
                                        <span class="direct-chat-name float-left">${item.user_name}</span>
                                        <span class="direct-chat-timestamp float-right">${formattedTime}</span>
                                    </div>
                                    <img class="direct-chat-img" src="{{ asset('dist/img/user3-128x128.jpg') }}" alt="message user image">
                                    <div class="direct-chat-text">
                                        ${item.message}
                                    </div>
                                </div>
                            `;

                            chatContainer.append(newMessage);
                        });

                        chatContainer.scrollTop(chatContainer[0].scrollHeight);
                    },
                    error: function(err) {
                        console.log(err);
                    }
                });
            }

            $('#btn-send-message').unbind('click').click(function() {
                var message = $('#message').val();

                $.ajax({
                    type: 'POST',
                    url: '{{ route('diskusi.sentMessage') }}',
                    data: {
                        channel: channel_id,
                        tags: tags_id,
                        message: message
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr("content")
                    },
                    success: function(response) {
                        $('#message').val('');
                        syncMessage();
                    },
                    error: function(error) {
                        console.log(error);
                    }
                });
            });

            @if (session('success'))
                Swal.fire({
                    icon: 'success',
                    title: 'Success...',
                    text: '{{ session('success') }}'
                });
            @endif

            @if (session('error'))
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: '{{ session('error') }}'
                });
            @endif
        });
    </script>
@endpush
