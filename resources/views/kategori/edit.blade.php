@extends('layout.app')

@section('title', 'Edit Kategori - ' . env('APP_NAME'))

@section('headBody')
    @include('includes.breadcrumb', [
        'title' => 'Edit Kategori'
    ])
@endsection

@push('styles')

@endpush

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <form action="{{ route('kategori.update', $kategori->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="card-body">
                        <div class="form-group">
                            <label>Nama Kategori</label>
                            <input type="text" class="form-control" name="nama_kategori" placeholder="Masukkan nama kategori" value="{{ $kategori->category_name }}">
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="{{ route('kategori.index') }}" class="btn btn-warning">Batal</a>
                        &nbsp;
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

@endpush
