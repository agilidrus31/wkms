@extends('layout.app')

@section('title', 'Create Kategori - ' . env('APP_NAME'))

@section('headBody')
    @include('includes.breadcrumb', [
        'title' => 'Create Kategori'
    ])
@endsection

@push('styles')

@endpush

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <form action="{{ route('kategori.store') }}" method="POST">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label>Nama Kategori</label>
                            <input type="text" class="form-control" name="nama_kategori" placeholder="Masukkan nama kategori">
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="{{ route('kategori.index') }}" class="btn btn-warning">Batal</a>
                        &nbsp;
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

@endpush
