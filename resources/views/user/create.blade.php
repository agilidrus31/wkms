@extends('layout.app')

@section('title', 'Create User - ' . env('APP_NAME'))

@section('headBody')
    @include('includes.breadcrumb', [
        'title' => 'Create User',
    ])
@endsection

@push('styles')
@endpush

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <form action="{{ route('user.store') }}" method="POST">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" class="form-control" name="name" placeholder="Masukkan nama">
                        </div>
                        <div class="form-group">
                            <label>Username</label>
                            <input type="text" class="form-control" name="username" placeholder="Masukkan username">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" name="email" placeholder="Masukkan email">
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control" name="password" placeholder="Masukkan password">
                        </div>
                        <div class="form-group">
                            <label>Jabatan</label>
                            <select class="form-control" name="jabatan">
                                <option value="">--Select Employee--</option>
                                @foreach ($jabatan as $j)
                                    <option value="{{ $j->id }}">{{ $j->jabatan_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="{{ route('user.index') }}" class="btn btn-warning">Batal</a>
                        &nbsp;
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script>
    $(function() {
        @if (session('success'))
                Swal.fire({
                    icon: 'success',
                    title: 'Success...',
                    text: '{{ session('success') }}'
                });
            @endif

            @if (session('error'))
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: '{{ session('error') }}'
                });
            @endif
    });
</script>
@endpush
