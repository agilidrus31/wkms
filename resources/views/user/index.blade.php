@extends('layout.app')

@section('title', 'User - ' . env('APP_NAME'))

@section('headBody')
    @include('includes.breadcrumb', [
        'title' => 'User',
    ])
@endsection

@push('styles')
@endpush

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="btn-group" style="margin-bottom: 1rem;">
                <a href="{{ route('user.create') }}" class="btn btn-primary btn-sm" style="width: 8rem;">
                    <i class="fa fa-plus"></i> &nbsp; &nbsp;
                    Add Data
                </a>
            </div>
            <div class="card">
                <div class="card-header">
                    <div class="card-tools">
                        <div class="input-group input-group-sm">
                            <select class="form-control float-right" name="jabatan" id="jabatan">
                                <option value="">--Select Employee--</option>
                                @foreach ($jabatan as $j)
                                    <option value="{{ $j->id }}">{{ $j->jabatan_name }}</option>
                                @endforeach
                            </select>
                            &nbsp; &nbsp; &nbsp;
                            <button type="button" class="btn btn-secondary btn-sm" id="btnReset">
                                Reset
                            </button>
                            &nbsp; &nbsp; &nbsp;
                            <button type="button" class="btn btn-secondary btn-sm" id="btnSearch">
                                Cari
                            </button>
                        </div>
                    </div>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-head-fixed text-nowrap table-striped table-hover datatable">
                        <thead>
                            <th>
                                #
                            </th>
                            <th>Name</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Jabatan</th>
                            <th>Aksi</th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(function() {
            var table = $('.datatable').DataTable({
                searching: false,
                destroy: true,
                processing: false,
                ordering: false,
                serverSide: true,
                ajax: {
                    url: "{{ route('user.index') }}",
                    data: function(d) {
                        (d.jabatan = $('#jabatan').val())
                    },
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'username',
                        name: 'username'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'jabatan_name',
                        name: 'jabatan_name'
                    },
                    {
                        data: 'action',
                        name: 'action'
                    }
                ],
            });

            @if (session('success'))
                Swal.fire({
                    icon: 'success',
                    title: 'Success...',
                    text: '{{ session('success') }}'
                });
            @endif

            @if (session('error'))
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: '{{ session('error') }}'
                });
            @endif
        });
    </script>
@endpush
