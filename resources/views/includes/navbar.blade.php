<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="javascript:void(0);" class="nav-link" style="font-size: 18px;"><b>Knowledge Management System</b></a>
        </li>
    </ul>

    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown no-arrow mx-1">
            <a class="nav-link dropdown-toggle text-dark" href="javascript:void(0);" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-fw fa-bell fa-lg"></i>
                <span id="show_jmlnew" class="mb-3"></span>
            </a>
            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                <h6 class="dropdown-header">
                    Notifikasi
                </h6>
                <div id="show_data"></div>
            </div>
        </li>

        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle text-dark" href="javascript:void(0);" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-fw fa-user mr-1"></i>
                <span class="mr-2 d-none d-lg-inline text-gray-700">
                    {{ Auth::user()->name }}
                </span>
                <i class="fas fa-angle-down"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item py-2" href="javascript:void(0);">
                    <i class="fas fa-id-card-alt fa-sm fa-fw mr-2 text-black-100"></i>
                    Profile Saya
                </a>
                <a class="dropdown-item py-2" href="javascript:void(0);">
                    <i class="fas fa-key fa-sm fa-fw mr-2 text-black-100"></i>
                    Ganti Password
                </a>
                <a class="dropdown-item py-2" href="#modal-logout" data-toggle="modal">
                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-black-100"></i>
                    Logout
                </a>
            </div>
        </li>
    </ul>
</nav>
