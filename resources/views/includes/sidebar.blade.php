<aside class="main-sidebar sidebar-light-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('dashboard.index') }}" class="brand-link">
        <img src="{{ asset('images/logo.jpg') }}" alt="logo" class="brand-image img-circle elevation-3" style="opacity: .8;">
        <span class="brand-text font-weight-light"><b>KMS</b></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{ auth()->user()->name }}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                data-accordion="false">
                <li class="nav-item">
                    <a href="{{ route('dashboard.index') }}"
                        class="nav-link {{ Request::routeIs('dashboard.index') ? 'active' : '' }}">
                        <i class="fas fa-tachometer-alt nav-icon"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('dokumen.index') }}"
                        class="nav-link {{ Request::routeIs('dokumen.index') ? 'active' : '' }}">
                        <i class="fas fa-file-alt nav-icon"></i>
                        <p>Dokumen</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('diskusi.index') }}"
                        class="nav-link {{ Request::routeIs('diskusi.index') ? 'active' : '' }}">
                        <i class="fas fa-comments nav-icon"></i>
                        <p>Diskusi</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('user.index') }}"
                        class="nav-link {{ Request::routeIs('user.index') ? 'active' : '' }}">
                        <i class="fas fa-users nav-icon"></i>
                        <p>User</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('jabatan.index') }}"
                        class="nav-link {{ Request::routeIs('jabatan.index') ? 'active' : '' }}">
                        <i class="fas fa-user-cog nav-icon"></i>
                        <p>Jabatan</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('kategori.index') }}"
                        class="nav-link {{ Request::routeIs('kategori.index') ? 'active' : '' }}">
                        <i class="fas fa-list nav-icon"></i>
                        <p>Kategori</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
