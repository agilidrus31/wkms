@extends('layout.app')

@section('title', 'Dokumen - ' . env('APP_NAME'))

@section('headBody')
    @include('includes.breadcrumb', [
        'title' => 'Dokumen',
    ])
@endsection

@push('styles')
@endpush

@section('content')
    <div class="row">
        <div class="col-lg-12">
            @if ($jabatan->upload_knowledge_document)
                <div class="btn-group" style="margin-bottom: 1rem;">
                    <a href="{{ route('dokumen.create') }}" class="btn btn-primary btn-sm" style="width: 8rem;">
                        <i class="fa fa-plus"></i> &nbsp; &nbsp;
                        Add Data
                    </a>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <div class="card-tools">
                        <div class="input-group input-group-sm">
                            @if (strtolower($jabatan->jabatan_name) != 'supervisor' && strtolower($jabatan->jabatan_name) != 'karyawan')
                                <select class="form-control float-right" name="status" id="status">
                                    <option value="">-- pilih status --</option>
                                    <option value="0">Belum Verifikasi</option>
                                    <option value="1">Sudah Verifikasi</option>
                                </select>
                                &nbsp; &nbsp;
                            @endif
                            Start Date &nbsp;
                            <input type="date" class="form-control float-right" name="stDate" id="stDate"
                                placeholder="Start Date" value="">
                            &nbsp; &nbsp;
                            End Date &nbsp;
                            <input type="date" class="form-control float-right" name="endDate" id="endDate"
                                placeholder="End Date" value="">
                            &nbsp; &nbsp;
                            <select class="form-control float-right" name="kategori" id="kategori">
                                <option value="">-- pilih kategori --</option>
                                @foreach ($kategori as $k)
                                    <option value="{{ $k->id }}">{{ $k->category_name }}</option>
                                @endforeach
                            </select>
                            &nbsp; &nbsp;
                            <button class="btn btn-secondary btn btn-sm" id="btnSearch">
                                Cari
                            </button>
                        </div>
                    </div>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-head-fixed text-nowrap table-striped table-hover datatable">
                        <thead>
                            <th>#</th>
                            <th>Nama Dokumen</th>
                            <th>Kategori</th>
                            <th>Dibuat</th>
                            <th>Is verified</th>
                            <th>Download</th>
                            <th>Aksi</th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(function() {
            var table = $('.datatable').DataTable({
                searching: false,
                destroy: true,
                processing: false,
                ordering: false,
                serverSide: true,
                ajax: {
                    url: "{{ route('dokumen.index') }}",
                    data: function(d) {
                        (d.stDate = $('#stDate').val()),
                        (d.endDate = $('#endDate').val()),
                        (d.kategori = $('#kategori').val()),
                        (d.status = $('#status').val())
                    }
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama_dokumen',
                        name: 'nama_dokumen'
                    },
                    {
                        data: 'category_name',
                        name: 'category_name'
                    },
                    {
                        data: 'created_by',
                        name: 'created_by'
                    },
                    {
                        data: 'is_verified',
                        name: 'is_verified'
                    },
                    {
                        data: 'download',
                        name: 'download'
                    },
                    {
                        data: 'action',
                        name: 'action'
                    }
                ],
            });

            $('#btnSearch').unbind('click').click(function() {
                table.draw();
            });

            $(document).on('click', '.btnShow', function(e) {
                e.preventDefault();
                var id = $(this).data('id');
                $.ajax({
                    url: "{{ route('dokumen.show', ':id') }}".replace(':id', id),
                    type: 'GET',
                    success: function(res) {
                        window.open(res.url, '_blank');
                    }
                });
            });

            $(document).on('click', '.btnSubmit', function(e) {
                e.preventDefault();
                Swal.fire({
                    title: 'Apakah anda yakin?',
                    text: "Data yang dihapus tidak dapat dikembalikan!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '<i class="fa fa-trash"></i> Ya, hapus!',
                }).then((result) => {
                    if (result.isConfirmed) {
                        var form = $(this).closest('form');
                        form.submit();
                    }
                });
            });

            @if (session('success'))
                Swal.fire({
                    icon: 'success',
                    title: 'Success...',
                    text: '{{ session('success') }}'
                });
            @endif

            @if (session('error'))
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: '{{ session('error') }}'
                });
            @endif
        });
    </script>
@endpush
