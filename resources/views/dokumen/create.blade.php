@extends('layout.app')

@section('title', 'Create Dokumen - ' . env('APP_NAME'))

@section('headBody')
    @include('includes.breadcrumb', [
        'title' => 'Create Dokumen'
    ])
@endsection

@push('styles')

@endpush

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <form action="{{ route('dokumen.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label>Nama Dokumen</label>
                            <input type="text" class="form-control" name="nama_dokumen" placeholder="Masukkan nama dokumen">
                        </div>
                        <div class="form-group">
                            <label>Kategori</label>
                            <select class="form-control" name="kategori">
                                <option value="">--Pilih Kategori--</option>
                                @foreach ($kategori as $k)
                                <option value="{{ $k->id }}">{{ $k->category_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>File</label>
                            <input type="file" class="form-control" name="file">
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="{{ route('dokumen.index') }}" class="btn btn-warning">Batal</a>
                        &nbsp;
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

@endpush
