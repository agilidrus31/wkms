<!DOCTYPE html>
<html lang="en">

<head>
    <title>@yield('title')</title>
    @include('includes.head')
    @stack('styles')
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        @include('includes.navbar')

        @include('includes.sidebar')

        <div class="content-wrapper">
            <div class="content-header">
                <div class="container-fluid">
                    @yield('headBody')
                </div>
            </div>

            <section class="content">
                <div class="container-fluid">
                    @yield('content')
                </div>
            </section>
        </div>

        <footer class="main-footer">
            <strong>Copyright by Agil Al Idrus</strong>
        </footer>

        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>

        <div class="modal fade" id="modal-logout" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form action="{{ route('logout') }}" method="POST" accept-charset="utf-8">
                        @csrf
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">
                                Are you sure want to logout?
                            </h5>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            Choose "Logout" button if yes.
                        </div>
                        <div class="modal-footer" id="modalFooter">
                            <button class="btn btn-primary" type="button" data-dismiss="modal">
                                Cancel
                            </button>
                            <button class="btn btn-danger" type="submit">
                                Log Out
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('includes.js')
    @stack('scripts')
</body>

</html>
